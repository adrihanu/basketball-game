# Basketball game

Script for scoring points and breaking records in https://basketball.frvr.com

The script detects basketball board and adjust mouse movment to score.

Tags: `Python`, `Bash`, `OpenCV`, `NumPy`

## Running

Install requirments, open browser window with the game and run:

```python
python3 main.py
```

## Game

![png](.img/game.png)
