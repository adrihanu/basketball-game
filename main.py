import cv2, mss, sys, time, numpy, imutils, subprocess
from PIL import Image

def distance(A,B): return numpy.sqrt((A[0]-B[0])**2 + (A[1]-B[1])**2)

def diagonal(rectangle):
    p1=(rectangle[0][0][0], rectangle[0][0][1])
    p2=(rectangle[1][0][0], rectangle[1][0][1])
    p3=(rectangle[2][0][0], rectangle[2][0][1])
    p4=(rectangle[3][0][0], rectangle[3][0][1])
    return max(distance(p1,p2),distance(p1,p3),distance(p1,p4),)

def getMouseID():
    mouseID=subprocess.check_output("xinput --list | grep -i -m 1 'mouse' | grep -o 'id=[0-9]\+' | grep -o '[0-9]\+'",shell=True).strip().decode("utf-8")
    return mouseID

def getMouseLocation():
    output=subprocess.check_output(["xdotool", "getmouselocation"]).split(b' ')
    x=int(output[0].strip(b'x:'))
    y=int(output[1].strip(b'y:'))
    return x, y

def getMouseInfo(mouseID):
    info=subprocess.check_output(["xinput", "--query-state", mouseID]).decode("utf-8").replace('\t',' ').replace('\n',' ').split(' ')
    info=[i for i in info if '=' in i]
    info=[i.split('=') for i in info]
    info={key: value for (key, value) in info}
    return info

def setROI():
    print('Set ROI')
    ROI=False
    while True:
        mouseLocation=getMouseLocation()
        mouseInfo = getMouseInfo(getMouseID())
        if not ROI and mouseInfo['button[1]']=='down':#first click
            ROI={'left': mouseLocation[0], 'top': mouseLocation[1], 'width': 100, 'height': 100}
        elif ROI and mouseInfo['button[1]']=='down':#update
            width = mouseLocation[0] - ROI['left']
            height = mouseLocation[1] - ROI['top']
            ROI['width'] = width if width>0 else 1
            ROI['height'] = height if height>0 else 1
        elif ROI and mouseInfo['button[1]']=='up':
            cv2.destroyAllWindows()
            break

        if ROI:
            image = numpy.array(sct.grab(ROI))
            cv2.imshow('image', image)# Display the picture
            cv2.waitKey(1)
    return ROI
def findBall():
    ball=(0,0,0)
    circles = cv2.HoughCircles(blurred,cv2.HOUGH_GRADIENT,1,80,param2=50,minRadius=30,maxRadius=60)
    if circles is not None:
        circles = numpy.round(circles[0, :]).astype("int")
        for (x, y, r) in circles:
            if r>ball[2]: ball=(x, y, r)
    return ball

def findBoard(ball):
    board=(0,0,0)
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    for c in cnts:
        M = cv2.moments(c)
        try:
            x = int(M["m10"] / M["m00"])
            y = int(M["m01"] / M["m00"])
        except: continue
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.04 * peri, True)
        cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
        if len(approx) == 4:

            d = int(diagonal(approx))
            ratio=ball[2]/d
            if 0.5<ratio<0.7 and y<ball[1]+10: board=(x,y,d)
    return board

def setExtremeValues():
    global minX, maxX
    minX=maxX=None
    while minX is None or maxX is None:
        lastDir = getDir()
        while True:
            print(getDir() )
            #Dir=getDir()
            #if Dir!=lastDir: break
        print('zmiana',lastDir)
        if lastDir=='left': minX=findBoard(ball)[0]
        else: maxX=findBoard(ball)[0]
    print('\nExtreme values set','Min:',minX,'Max:',maxX)
    return minX,maxX

def shoot(ball, board, move=0):
    board=(board[0]+move,board[1],board[2])
    hh=(ball[1]-board[1])/ball[2]
    ww=abs((board[0]-ball[0])/ball[2])

    k=0.3
    #else: k=0.2

    v=int(k*(board[0]-ball[0]))
    h=int(k*(board[1]-ball[1]))
    print('\r'+80*' '+'\rhh: {0:.2f} ww:{1:.1f} k:{2:.1f}'.format(hh,ww,k))
    subprocess.Popen("xdotool mousemove --sync "+str(ROI['left']+ball[0])+' '+str(ROI['top']+ball[1])+';sleep 0.1', shell=True).communicate()
    subprocess.Popen("xdotool mousedown 1 mousemove_relative --sync -- " +str(v)+' '+str(h), shell=True).communicate()
    subprocess.Popen("xdotool mouseup 1; sleep 2", shell=True).communicate()

def display(msg):
    sys.stdout.write('\r{}\r{}'.format(80*" ", msg))
    sys.stdout.flush()
def getImage():
    global image,gray,blurred,thresh
    image = numpy.array(sct.grab(ROI))
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    thresh=cv2.inRange(blurred, 70,120)
def getDir():
    getImage()
    lastX=findBoard(ball)[0]
    time.sleep(0.1)
    getImage()
    if lastX-findBoard(ball)[0]>0: return 'left'
    elif lastX-findBoard(ball)[0]<0: return 'right'
    else: getDir()
with mss.mss() as sct:
    boardMoving=False
    ROI=setROI()
    ball=board=lastBall=lastBoard=(0,0,0)
    left=right=False
    confirm=0
    lastDir=''
    minX=maxX=None
    lastX=0
    getImage()
    firstBall = findBall()#find ball
    while True:
        getImage()
        ball = findBall()#find ball
        cv2.circle(image, (ball[0], ball[1]), 5, (0, 255, 0), 4)

        board = findBoard(ball)#find board
        #if not all(ball): subprocess.Popen("xdotool mousemove --sync "+str(ROI['left']+firstBall[0])+' '+str(ROI['top']+firstBall[1]+50)+' click 1;sleep 2', shell=True).communicate()
        cv2.circle(image, (board[0], board[1]), 5, (0, 255, 0), 4)
        cv2.line(image, ball[:2], board[:2], (222,222,222),4)
        if all(board):#if both on the screen

            cv2.circle(image, (board[0], board[1]), 5, (0, 255, 0), 4)
            cv2.line(image, ball[:2], board[:2], (222,222,222),4)
            cv2.putText(image, 'Distance: {0:.2f}'.format(distance(ball,board)/ball[2]), (40, 40), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,0), 3)

            if not boardMoving:
                if confirm ==3:
                    display('Seting extreme values.')
                    setExtremeValues()
                    boardMoving=True
                    confirm=0
                    move=int((13/12)*(maxX-minX)/4)
                elif confirm<-5:
                    confirm=0
                    shoot(ball,board);
                elif board[0] != lastBoard[0]:#board position changed
                    display('Confirming moving board: '+str(confirm)+' / 2')
                    confirm+=1
                    time.sleep(1)
                else:
                    confirm-=1##board position didnt change
                    display('Confirming static board:: '+str(confirm)+' / -5')

            else: #board is moving
                while 1: print(getDir())
                Dir=getDir()
                print(Dir)
                if Dir=='left':
                    confirm=0
                    if board[0]>minX+move: shoot(ball,board,-move)
                    left=right=0
                else:
                    confirm=0
                    if board[0]<maxX-move: shoot(ball,board,move)
                    left=right=0


                if confirm<-100:
                    print('\n',20*'=','\n')
                    minX=maxX=None
                    boardMoving=False
                #===== ===== ===== ===== =====
            lastBoard=board
            lastBall=ball
        else: display('Waiting for objects...')
        #cv2.imshow('image', image)# Display the picture
        #cv2.waitKey(15)
'''
#http://basketball.frvr.com/
#https://python-mss.readthedocs.io/en/dev/examples.html
'''
